import axios from "axios";

import {
  ADD_ENTRY,
  GET_ERRORS,
  GET_ENTRIES,
  GET_ENTRY,
  ENTRY_LOADING,
  CLEAR_ERRORS,
  DELETE_ENTRY
} from "./types";

// Add Entry
export const addEntry = (competitionId, entryData) => dispatch => {
  dispatch(clearErrors());
  axios
    .post(`/api/entries/postEntry/${competitionId}`, entryData)
    .then(res =>
      dispatch({
        type: ADD_ENTRY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Get Entries
export const getEntries = id => dispatch => {
  dispatch(setEntryLoading());
  axios
    .get(`/api/entries/${id}`)
    .then(res =>
      dispatch({
        type: GET_ENTRIES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ENTRIES,
        payload: null
      })
    );
};

// Get Entries sortedf by likes
export const getEntriesLikes = id => dispatch => {
  dispatch(setEntryLoading());
  axios
    .get(`/api/entries/likesort/${id}`)
    .then(res =>
      dispatch({
        type: GET_ENTRIES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ENTRIES,
        payload: null
      })
    );
};

// Get Entry
export const getEntry = id => dispatch => {
  dispatch(setEntryLoading());
  axios
    .get(`/api/entries/entry/${id}`)
    .then(res =>
      dispatch({
        type: GET_ENTRY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ENTRY,
        payload: null
      })
    );
};

// Delete Entry
export const deleteEntry = id => dispatch => {
  dispatch(clearErrors());
  axios
    .delete(`/api/entries/delete/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_ENTRY,
        payload: id
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Like to Entry
export const addLike = id => dispatch => {
  dispatch(clearErrors());
  axios
    .post(`/api/entries/like/${id}`)
    .then(res => dispatch(getEntry()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Remove Like from Entry
export const removeLike = id => dispatch => {
  dispatch(clearErrors());
  axios
    .post(`/api/entries/unlike/${id}`)
    .then(res => dispatch(getEntry()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Set loading state for get entries
export const setEntryLoading = () => {
  return {
    type: ENTRY_LOADING
  };
};
// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
