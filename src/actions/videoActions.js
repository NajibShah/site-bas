import axios from "axios";

import {
  ADD_VIDEO,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_VIDEOS,
  GET_VIDEO,
  VIDEO_LOADING,
  DELETE_VIDEO
} from "./types";

// Add Video
export const addVideo = videoData => dispatch => {
  axios
    .post("/api/videos/uploadBACK", videoData)
    .then(res =>
      dispatch({
        type: ADD_VIDEO,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Get Videos
export const getVideos = () => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get("/api/videos")
    .then(res =>
      dispatch({
        type: GET_VIDEOS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEOS,
        payload: null
      })
    );
};

// // GET VIDEOS BY PROFILE ROUTES
// export const getVideosProfile = profile => dispatch => {
//   dispatch(setVideoLoading());
//   axios
//     .get(`/api/videos/${profile}`)
//     .then(res =>
//       dispatch({
//         type: GET_VIDEOS,
//         payload: res.data
//       })
//     )
//     .catch(err =>
//       dispatch({
//         type: GET_VIDEOS,
//         payload: null
//       })
//     );
// };

// GET VIDEOS BY GENRES ROUTES

// Get Videos by Genre : Horror
export const getVideosHorror = () => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get("/api/videos/genrehorror")
    .then(res =>
      dispatch({
        type: GET_VIDEOS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEOS,
        payload: null
      })
    );
};
// Get Videos by Genre : Comedy
export const getVideosComedy = () => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get("/api/videos/genrecomedy")
    .then(res =>
      dispatch({
        type: GET_VIDEOS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEOS,
        payload: null
      })
    );
};
// Get Videos by Genre : Sci-Fi
export const getVideosSciFi = () => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get("/api/videos/genrescifi")
    .then(res =>
      dispatch({
        type: GET_VIDEOS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEOS,
        payload: null
      })
    );
};
// Get Videos by Genre : Documentary
export const getVideosDocumentary = () => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get("/api/videos/genredocumentary")
    .then(res =>
      dispatch({
        type: GET_VIDEOS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEOS,
        payload: null
      })
    );
};
// Get Videos by Genre : Thriller
export const getVideosThriller = () => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get("/api/videos/genrethriller")
    .then(res =>
      dispatch({
        type: GET_VIDEOS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEOS,
        payload: null
      })
    );
};
// Get Videos by Genre : Suspense
export const getVideosSuspense = () => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get("/api/videos/genresuspense")
    .then(res =>
      dispatch({
        type: GET_VIDEOS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEOS,
        payload: null
      })
    );
};
// Get Videos by Genre : RomCom
export const getVideosRomCom = () => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get("/api/videos/genreromcom")
    .then(res =>
      dispatch({
        type: GET_VIDEOS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEOS,
        payload: null
      })
    );
};

// Get Video by id
export const getVideo = id => dispatch => {
  dispatch(setVideoLoading());
  axios
    .get(`/api/videos/${id}`)
    .then(res =>
      dispatch({
        type: GET_VIDEO,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VIDEO,
        payload: null
      })
    );
};

// Delete Video
export const deleteVideo = id => dispatch => {
  axios
    .delete(`/api/videos/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_VIDEO,
        payload: id
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Like
export const addLike = id => dispatch => {
  axios
    .post(`/api/videos/like/${id}`)
    .then(res => dispatch(getVideos()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Remove Like
export const removeLike = id => dispatch => {
  axios
    .post(`/api/videos/unlike/${id}`)
    .then(res => dispatch(getVideos()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Comment
export const addComment = (videoId, commentData) => dispatch => {
  dispatch(clearErrors());
  axios
    .post(`/api/videos/comment/${videoId}`, commentData)
    .then(res =>
      dispatch({
        type: GET_VIDEO,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete Comment
export const deleteComment = (videoId, commentId) => dispatch => {
  axios
    .delete(`/api/videos/comment/${videoId}/${commentId}`)
    .then(res =>
      dispatch({
        type: GET_VIDEO,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Set loading state for get videos
export const setVideoLoading = () => {
  return {
    type: VIDEO_LOADING
  };
};

// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
