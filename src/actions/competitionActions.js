import axios from "axios";

import {
  ADD_COMPETITION,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_COMPETITIONS,
  GET_COMPETITION,
  COMPETITION_LOADING,
  DELETE_COMPETITION
} from "./types";

// Add Competition
export const addCompetition = competitionData => dispatch => {
  axios
    .post("/api/competitions/uploadBACK", competitionData)
    .then(res =>
      dispatch({
        type: ADD_COMPETITION,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// // Add Entry
// export const addEntry = (competitionId, entryData) => dispatch => {
//   dispatch(clearErrors());
//   axios
//     .post(`/api/entries/postEntry/${competitionId}`, entryData)
//     .then(res =>
//       dispatch({
//         type: GET_COMPETITION,
//         payload: res.data
//       })
//     )
//     .catch(err =>
//       dispatch({
//         type: GET_ERRORS,
//         payload: err.response.data
//       })
//     );
// };

// Get Competitions
export const getCompetitions = () => dispatch => {
  dispatch(setCompetitionLoading());
  axios
    .get("/api/competitions")
    .then(res =>
      dispatch({
        type: GET_COMPETITIONS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_COMPETITIONS,
        payload: null
      })
    );
};

// Get Competition
export const getCompetition = id => dispatch => {
  dispatch(setCompetitionLoading());
  axios
    .get(`/api/competitions/${id}`)
    .then(res =>
      dispatch({
        type: GET_COMPETITION,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_COMPETITION,
        payload: null
      })
    );
};

// Delete Competition
export const deleteCompetition = id => dispatch => {
  axios
    .delete(`/api/competitions/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_COMPETITION,
        payload: id
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Like
export const addLike = id => dispatch => {
  axios
    .post(`/api/competitions/like/${id}`)
    .then(res => dispatch(getCompetitions()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Remove Like
export const removeLike = id => dispatch => {
  axios
    .post(`/api/competitions/unlike/${id}`)
    .then(res => dispatch(getCompetitions()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Comment
export const addComment = (competitionId, commentData) => dispatch => {
  dispatch(clearErrors());
  axios
    .post(`/api/competitions/comment/${competitionId}`, commentData)
    .then(res =>
      dispatch({
        type: GET_COMPETITION,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete Comment
export const deleteComment = (competitionId, commentId) => dispatch => {
  axios
    .delete(`/api/competitions/comment/${competitionId}/${commentId}`)
    .then(res =>
      dispatch({
        type: GET_COMPETITION,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Set loading state for get competitions
export const setCompetitionLoading = () => {
  return {
    type: COMPETITION_LOADING
  };
};

// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
