import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import profileReducer from "./profileReducer";
import postReducer from "./postReducer";
//imported later
import videoReducer from "./videoReducer";
import competitionReducer from "./competitionReducer";
import entryReducer from "./entryReducer";

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  profile: profileReducer,
  post: postReducer,
  //made later
  video: videoReducer,
  competition: competitionReducer,
  entry: entryReducer
});
