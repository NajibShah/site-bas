import {
  ADD_ENTRY,
  GET_ENTRIES,
  GET_ENTRY,
  DELETE_ENTRY,
  ENTRY_LOADING
} from "../actions/types";

const initialState = {
  entries: [],
  entry: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ENTRY_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_ENTRIES:
      return {
        ...state,
        entries: action.payload,
        loading: false
      };
    case GET_ENTRY:
      return {
        ...state,
        entry: action.payload,
        loading: false
      };
    case ADD_ENTRY:
      return {
        ...state,
        entries: [action.payload, ...state.entries]
      };
    case DELETE_ENTRY:
      return {
        ...state,
        entries: state.entries.filter(entry => entry._id !== action.payload)
      };
    default:
      return state;
  }
}
