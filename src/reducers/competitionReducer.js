import {
  ADD_COMPETITION,
  GET_COMPETITIONS,
  GET_COMPETITION,
  DELETE_COMPETITION,
  COMPETITION_LOADING
} from "../actions/types";
const initialState = {
  competitions: [],
  competition: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case COMPETITION_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_COMPETITIONS:
      return {
        ...state,
        competitions: action.payload,
        loading: false
      };
    case ADD_COMPETITION:
      return {
        ...state,
        competitions: [action.payload, ...state.competitions]
      };
    case GET_COMPETITION:
      return {
        ...state,
        competition: action.payload,
        loading: false
      };
    case DELETE_COMPETITION:
      return {
        ...state,
        competitions: state.competitions.filter(
          competition => competition._id !== action.payload
        )
      };
    default:
      return state;
  }
}
