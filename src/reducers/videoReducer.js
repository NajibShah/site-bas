import {
  ADD_VIDEO,
  GET_VIDEOS,
  GET_VIDEO,
  DELETE_VIDEO,
  VIDEO_LOADING
} from "../actions/types";
const initialState = {
  videos: [],
  video: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case VIDEO_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_VIDEOS:
      return {
        ...state,
        videos: action.payload,
        loading: false
      };
    case ADD_VIDEO:
      return {
        ...state,
        videos: [action.payload, ...state.videos]
      };
    case GET_VIDEO:
      return {
        ...state,
        video: action.payload,
        loading: false
      };
    case DELETE_VIDEO:
      return {
        ...state,
        videos: state.videos.filter(video => video._id !== action.payload)
      };
    default:
      return state;
  }
}
