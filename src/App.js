import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
/*mimic standard server, so we're able to use back button etc and be able to go back anbd forth. 
BrowserRouter as Router so we use Router instead of Browser Router.*/
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';
import { clearCurrentProfile } from './actions/profileActions';

import { Provider } from 'react-redux';
import store from './store';

import PrivateRoute from './components/common/PrivateRoute';

import Navbar from './components/navbar-and-footer/Navbar';
import Footer from './components/navbar-and-footer/Footer';
import Landing from './pages/landing/landing.component';

import Register from './pages/login-and-register/Register';
import Login from './pages/login-and-register/Login';

import Dashboard from './pages/dashboard/Dashboard';
import CreateProfile from './pages/dashboard/create-profile/CreateProfile';
import EditProfile from './pages/dashboard/edit-profile/EditProfile';
import AddExperience from './pages/dashboard/edit-profile/AddExperience';
import AddEducation from './pages/dashboard/edit-profile/AddEducation';

import Blog from './components/blog/Blog';

import Profiles from './pages/user-pages/profiles/Profiles';
import Profile from './pages/user-pages/profile/Profile';

import Posts from './pages/user-pages/posts/Posts';
import Post from './pages/user-pages/post/Post';
import NotFound from './pages/not-found/NotFound';

import AdminLanding from './pages/admin-pages/adminLanding';
import UploadForm1 from './pages/admin-pages/upload-video/UploadForm1';
import NewComp from './pages/admin-pages/upload-competition/NewComp';
import DeleteComp from './pages/admin-pages/delete-competition/DeleteComp';
import DeleteVideo from './pages/admin-pages/delete-video/DeleteVideo';
import DeleteUser from './pages/admin-pages/delete-user/DeleteUser';

import UpToComp from './pages/user-pages/competition/upload-entry/SubmitEntry';

import UploadForm2 from './pages/user-pages/video/upload-video/UploadForm2';
import Success from './pages/user-pages/video/upload-video/Success';

import Videos2 from './pages/user-pages/video/video-feed/Videos2';
// import VideoItem2 from "./components/videos/VideoItem2";
import VideoBrad from './pages/user-pages/video/video-display/VideoBrad';

import CompLanding from './pages/user-pages/competition/comp-landing/CompLanding';
import Competition from './pages/user-pages/competition/comp-display/Competition';
import VideoEntry from './pages/user-pages/competition/entry-display/VideoEntry';

import ResLanding from './pages/user-pages/competition/result-landing/ResLanding';
import Result from './pages/user-pages/competition/result-display/Result';

// import album from "./components/discover/album";
// import genres from "./components/discover/genre";
// import length from "./components/discover/length";

import horror from './pages/user-pages/video/video-feed/genre-pages/VidGenHorror';
import comedy from './pages/user-pages/video/video-feed/genre-pages/VidGenComedy';
import scifi from './pages/user-pages/video/video-feed/genre-pages/VidGenSciFi';
import documentary from './pages/user-pages/video/video-feed/genre-pages/VidGenDocumentary';
import thriller from './pages/user-pages/video/video-feed/genre-pages/VidGenThriller';
import suspense from './pages/user-pages/video/video-feed/genre-pages/VidGenSuspense';
import romcom from './pages/user-pages/video/video-feed/genre-pages/VidGenRomCom';

import './App.css';

//Check for token so it can always check to see if its logged in. so refreshing a page will not log it out
if (localStorage.jwtToken) {
  //Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  //Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  //Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  //Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    //Logout user
    store.dispatch(logoutUser());
    // clear current profile
    store.dispatch(clearCurrentProfile());
    //redirect to login
    window.location.href = '/login';
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className='App'>
            <Navbar />
            {/* <Route exact path="/" component={Landing} /> */}
            <Route exact path='/' component={Login} />

            <div className='container1'>
              <Route exact path='/register' component={Register} />
              <Route exact path='/login' component={Login} />
              <Route exact path='/profiles' component={Profiles} />
              <Route exact path='/blog' component={Blog} />
              <Route exact path='/profile/:handle' component={Profile} />
              {/* Admin routes */}
              <Switch>
                <PrivateRoute
                  exact
                  path='/adminLanding'
                  component={AdminLanding}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/UploadForm1'
                  component={UploadForm1}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/uploadcompetition'
                  component={NewComp}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/deletecompetition'
                  component={DeleteComp}
                />
              </Switch>

              <Switch>
                <PrivateRoute
                  exact
                  path='/deletevideo'
                  component={DeleteVideo}
                />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/deleteuser' component={DeleteUser} />
              </Switch>
              {/* Dashboard Routes */}
              <Switch>
                <PrivateRoute exact path='/dashboard' component={Dashboard} />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/create-profile'
                  component={CreateProfile}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/edit-profile'
                  component={EditProfile}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/add-experience'
                  component={AddExperience}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/add-education'
                  component={AddEducation}
                />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/post/:id' component={Post} />
              </Switch>
              {/* Video Routes */}
              <Route exact path='/not-found' component={NotFound} />
              <Switch>
                <PrivateRoute
                  exact
                  path='/UploadForm2'
                  component={UploadForm2}
                />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/Success' component={Success} />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/feed' component={Posts} />
              </Switch>
              {/* <Switch>
                <PrivateRoute exact path="/discover" component={album} />
              </Switch> */}
              <Switch>
                <PrivateRoute exact path='/video/:id' component={VideoBrad} />
              </Switch>

              <Switch>
                <PrivateRoute exact path='/videos2' component={Videos2} />
              </Switch>
              {/* <Switch>
                <PrivateRoute exact path="/videoitem2" component={VideoItem2} />
              </Switch> */}
              {/* <Switch>
                <PrivateRoute exact path="/length" component={length} />
              </Switch> */}
              {/* genre routes */}
              {/* <Switch>
                <PrivateRoute exact path="/discover/genre" component={genres} />
              </Switch> */}
              <Switch>
                <PrivateRoute exact path='/horror' component={horror} />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/comedy' component={comedy} />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/sci-fi' component={scifi} />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/documentary'
                  component={documentary}
                />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/thriller' component={thriller} />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/suspense' component={suspense} />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/romantic comedy'
                  component={romcom}
                />
              </Switch>

              {/* Competitions Routes */}
              <Switch>
                <PrivateRoute
                  exact
                  path='/competitionslanding'
                  component={CompLanding}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/competitions/:id'
                  component={Competition}
                />
              </Switch>
              {/*ENTRIES*/}
              <Switch>
                <PrivateRoute exact path='/submitentry' component={UpToComp} />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path='/entries/entry/:id'
                  component={VideoEntry}
                />
              </Switch>
              {/*RESULTS*/}
              <Switch>
                <PrivateRoute
                  exact
                  path='/resultslanding'
                  component={ResLanding}
                />
              </Switch>
              <Switch>
                <PrivateRoute exact path='/results/:id' component={Result} />
              </Switch>
            </div>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
