import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import PostForm from "../../../components/posts/PostForm";
import PostFeed from "../../../components/posts/PostFeed";
import Spinner from "../../../components/common/Spinner";
import { getPosts } from "../../../actions/postActions";

import Typography from "@material-ui/core/Typography";

class Posts extends Component {
  componentDidMount() {
    this.props.getPosts();
  }

  render() {
    const { posts, loading } = this.props.post;
    const { auth } = this.props;
    let postContent;

    if (posts === null || loading) {
      postContent = <Spinner />;
    } else {
      postContent = <PostFeed posts={posts} />;
    }

    return (
      <div>
        <div className="heroUnit">
          <div className="heroContent">
            <Typography
              component="h1"
              variant="h3"
              align="center"
              style={{ color: "white" }}
              gutterBottom
            >
              Filmabad Blog!{" "}
            </Typography>
            <Typography
              variant="h8"
              align="center"
              style={{ color: "pink" }}
              paragraph
            >
              This is where filmabad writes about its log of competitions and
              website updates.
            </Typography>
          </div>
        </div>
        <div className="feed">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                {auth.user.name === "Filmabad" ? [<PostForm />] : null}
                {/* <PostForm /> */}
                {postContent}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Posts.propTypes = {
  getPosts: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  post: state.post,
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { getPosts }
)(Posts);
