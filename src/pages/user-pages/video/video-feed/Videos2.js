import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import VideoFeed2 from "../../../../components/videos/VideoFeed2";

import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

import Spinner from "../../../../components/common/Spinner";
import { getVideos } from "../../../../actions/videoActions";
import "./videos.css";

import { Tabs } from "antd";

const { TabPane } = Tabs;

class Videos2 extends Component {
  state = { size: "small" };

  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  componentDidMount() {
    this.props.getVideos();
  }

  render() {
    const { videos, loading } = this.props.video;
    let videoContent;
    const { size } = this.state;

    if (videos === null || loading) {
      videoContent = <Spinner />;
    } else {
      videoContent = <VideoFeed2 videos={videos} />;
    }

    return (
      <div>
        <div className="heroUnit">
          <div className="heroContent">
            <Typography
              component="h1"
              variant="h3"
              align="center"
              style={{ color: "white" }}
              gutterBottom
            >
              Discover
            </Typography>
            <Typography
              variant="h8"
              align="center"
              style={{ color: "pink" }}
              paragraph
            >
              This where you can discover amazing work by the most skilled
              video-craftsmen, contents, the creator.
              <br />
              To view videos categorized by Genre.
            </Typography>
            <br />
            <div className="heroButtons">
              <Grid container spacing={16} justify="center">
                <Grid item>
                  <Link to="horror">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      horror
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="comedy">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Comedy{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="sci-fi">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Sci-Fi{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="documentary">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Documentary{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="thriller">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Thriller{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="suspense">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Suspense{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="romantic comedy">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Romantic Comedy{" "}
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
        {/* <div className="heroUnit">
          <div className="heroContent">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              Discover
            </Typography>
            <Typography
              variant="h6"
              align="center"
              color="textSecondary"
              paragraph
            >
              This where you can discover amazing work by the most skilled
              video-craftsmen, contents, the creator.
              <br />
              To view videos categorized by Genre, or by Likes Click here.
            </Typography>
            <div className="heroButtons">
              <Grid container spacing={16} justify="center">
                <Grid item>
                  <Button variant="contained" color="primary">
                    <Link to="discover/genre">By Genre</Link>
                  </Button>
                </Grid>
                <Grid item>
                  <Link to="dicover/length">
                    <Button variant="outlined" color="primary">
                      Top Rated
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </div>
          </div>
        </div> */}
        <br />
        <Tabs defaultActiveKey="1" size={size}>
          <TabPane tab="New Uploads" key="1">
            <div className="feed">
              <div className="container">
                <div className="row">{videoContent}</div>
              </div>
            </div>
          </TabPane>
        </Tabs>
      </div>
    );
  }
}

Videos2.propTypes = {
  getVideos: PropTypes.func.isRequired,
  video: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  video: state.video,
});

export default connect(
  mapStateToProps,
  { getVideos }
)(Videos2);
