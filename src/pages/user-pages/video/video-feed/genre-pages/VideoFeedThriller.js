import React, { Component } from "react";
import PropTypes from "prop-types";
import VideoItem2 from "../../../../../components/videos/VideoItem2";

class VideoFeedThriller extends Component {
  render() {
    const { videos1 } = this.props;
    return videos1.map((video) => <VideoItem2 key={video._id} video={video} />);
  }
}

VideoFeedThriller.propTypes = {
  videos1: PropTypes.array.isRequired,
};

export default VideoFeedThriller;
