import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import VideoFeedComedy from "./VideoFeedComedy";
import Spinner from "../../../../../components/common/Spinner";
import { getVideosComedy } from "../../../../../actions/videoActions";
import "./../videos.css";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

import { Tabs } from "antd";
const { TabPane } = Tabs;

class VidGenComedy extends Component {
  state = { size: "small" };

  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  componentDidMount() {
    this.props.getVideosComedy();
  }

  render() {
    const { videos, loading } = this.props.video;
    let videoContent;
    const { size } = this.state;

    if (videos === null || loading) {
      videoContent = <Spinner />;
    } else {
      videoContent = <VideoFeedComedy videos1={videos} />;
    }

    return (
      <div>
        <div className="heroUnit">
          <div className="row">
            <div className="col-md-6">
              <br />
              <Link
                to="/videos2"
                className="btn btn-light mb-3 float-left"
                style={{ color: "gray" }}
              >
                Back To Discover
              </Link>
            </div>
            <div className="col-md-6" />
          </div>
          <div className="heroContentGenres">
            <Typography
              component="h1"
              variant="h3"
              align="center"
              style={{ color: "white" }}
              gutterBottom
            >
              Discover
            </Typography>
            <Typography
              variant="h8"
              align="center"
              style={{ color: "pink" }}
              paragraph
            >
              This where you can discover amazing work by the most skilled
              video-craftsmen, contents, the creator.
              <br />
              To view videos categorized by Genre.
            </Typography>
            <br />
            <div className="heroButtons">
              <Grid container spacing={16} justify="center">
                <Grid item>
                  <Link to="horror">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      horror
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="comedy">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Comedy{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="sci-fi">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Sci-Fi{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="documentary">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Documentary{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="thriller">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Thriller{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="suspense">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Suspense{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="romantic comedy">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Romantic Comedy{" "}
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
        <br />
        <Tabs defaultActiveKey="1" size={size}>
          <TabPane tab="Comedy Videos" key="1">
            <div className="feed">
              <div className="container">
                <div className="row">{videoContent}</div>
              </div>
            </div>
          </TabPane>
        </Tabs>
      </div>
    );
  }
}

VidGenComedy.propTypes = {
  getVideosComedy: PropTypes.func.isRequired,
  video: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  video: state.video,
});

export default connect(
  mapStateToProps,
  { getVideosComedy }
)(VidGenComedy);
