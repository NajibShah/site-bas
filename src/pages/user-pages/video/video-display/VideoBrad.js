import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import CommentForm from '../../../../components/video/CommentForm';
import CommentFeed from '../../../../components/video/CommentFeed';
import Spinner from '../../../../components/common/Spinner';
import {
  getVideo,
  addLike,
  removeLike,
} from '../../../../actions/videoActions';
import './scss/video-react.scss'; // import css
import classnames from 'classnames';
import './videoItem.css';

import { Typography } from 'antd';
import { Player, BigPlayButton } from 'video-react';

// import { Card } from "antd";
// import { Icon, Avatar, Comment, Tooltip, List } from "antd";

// const { Meta } = Card;
const { Text } = Typography;

class VideoBrad extends Component {
  componentDidMount() {
    this.props.getVideo(this.props.match.params.id);
  }
  onLikeClick(id) {
    this.props.addLike(id);
  }

  onUnlikeClick(id) {
    this.props.removeLike(id);
  }

  findUserLike(likes) {
    const { auth } = this.props;
    if (likes.filter((like) => like.user === auth.user.id).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    const { video, loading } = this.props.video;
    let videoContent;

    if (video === null || loading || Object.keys(video).length === 0) {
      videoContent = <Spinner />;
    } else {
      videoContent = (
        <div className='videoitembg'>
          <div className='bg-transparent'>
            <br />
            <Link to='/videos2' className='btn btn-light mb-3'>
              Back to Discover
            </Link>
            <div className='heroContentGenres'>
              <Typography
                component='h1'
                variant='h2'
                align='center'
                style={{ color: 'white' }}
                gutterBottom
              >
                {video.title}
              </Typography>
              <div className='col-md-8 offset-md-2'>
                <Typography
                  variant='h6'
                  align='center'
                  style={{
                    color: 'white',
                    paddingLeft: '40px',
                    paddingRight: '40px',
                  }}
                  paragraph
                >
                  {video.description}
                </Typography>
              </div>
              <br />
              <Typography
                variant='h6'
                align='center'
                style={{ color: 'pink' }}
                paragraph
              >
                Uploaded by: &nbsp;
                <Link to={`/profile/${video.name}`}>
                  <Text strong style={{ color: 'pink' }}>
                    {video.name}
                  </Text>
                </Link>
              </Typography>
              <Typography
                variant='h6'
                align='center'
                style={{ color: 'pink' }}
                paragraph
              >
                Genre: &nbsp;
                <Link to={`/${video.genre}`}>
                  <Text strong style={{ color: 'pink' }}>
                    {video.genre}
                  </Text>
                </Link>
              </Typography>
            </div>
          </div>

          <div className='post row'>
            <div className=' col-md-1' />

            <div className='videoplayer col-md-6'>
              <Player>
                <source
                  src={require(`../../../../useruploads/${video.videopath}`)}
                />
                <BigPlayButton position='center' />
              </Player>
              <br />
              <br />
              <br />
              <br />
            </div>
            <div className=' col-md-1' />
            <div className='col-md-3'>
              {/* <br />
              <Typography variant="h6" style={{ color: "gray" }} paragraph>
                Uploaded by: &nbsp;
                <Link to={`/profile/${video.name}`}>
                  <Text strong style={{ color: "black" }}>
                    {video.name}
                  </Text>
                </Link>
              </Typography>
              <Typography variant="h6" style={{ color: "gray" }} paragraph>
                Genre: &nbsp;
                <Link to={`/${video.genre}`}>
                  <Text strong style={{ color: "black" }}>
                    {video.genre}
                  </Text>
                </Link>
              </Typography>*/}
              <br />
              <br />
              <h6 style={{ color: 'white' }}>
                {/* (To like or Unlike this video click here) */}
                Like &nbsp;
                <button
                  onClick={this.onLikeClick.bind(this, video._id)}
                  type='button'
                  className='btn btn-light mr-1'
                >
                  <i
                    className={classnames('fas fa-thumbs-up', {
                      'text-info': this.findUserLike(video.likes),
                    })}
                  />
                  <span className='badge badge-light'>
                    {video.likes.length}
                  </span>
                </button>
                &nbsp; &nbsp;Unlike &nbsp;
                <button
                  onClick={this.onUnlikeClick.bind(this, video._id)}
                  type='button'
                  className='btn btn-light mr-1'
                >
                  <i className='text-secondary fas fa-thumbs-down' />
                </button>
              </h6>
              {/* <h4>
                <Text>Comments:</Text>
              </h4> */}
              <br />
              <CommentForm videoId={video._id} />
              <CommentFeed videoId={video._id} comments={video.comments} />
            </div>

            {/* <div className="container">
              <div className="row">
                <div className="col-md-3">
                  <br />
                  <h6>
                    (To like or Unlike this video click here)
                    <button
                      onClick={this.onLikeClick.bind(this, video._id)}
                      type="button"
                      className="btn btn-light mr-1"
                    >
                      <i
                        className={classnames("fas fa-thumbs-up", {
                          "text-info": this.findUserLike(video.likes)
                        })}
                      />
                      <span className="badge badge-light">
                        {video.likes.length}
                      </span>
                    </button>
                    <button
                      onClick={this.onUnlikeClick.bind(this, video._id)}
                      type="button"
                      className="btn btn-light mr-1"
                    >
                      <i className="text-secondary fas fa-thumbs-down" />
                    </button>
                  </h6>
                  <h4>
                    <Text>Comments:</Text>
                  </h4>
                  <CommentForm videoId={video._id} />
                  <CommentFeed videoId={video._id} comments={video.comments} />
                </div>
              </div>
            </div> */}
          </div>
        </div>
      );
    }

    return (
      <div>
        {videoContent}
        {/* <div className="post">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <br />
                <Link to="/videos2" className="btn btn-light mb-3">
                  Back to Discover
                </Link>
                {videoContent}
              </div>
            </div>
          </div>
        </div> */}
      </div>
    );
  }
}

VideoBrad.propTypes = {
  getVideo: PropTypes.func.isRequired,
  addLike: PropTypes.func.isRequired,
  removeLike: PropTypes.func.isRequired,
  video: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  video: state.video,
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { getVideo, addLike, removeLike }
)(VideoBrad);
