import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProfileHeader from '../../../components/profile/ProfileHeader';
import ProfileAbout from '../../../components/profile/ProfileAbout';
import ProfileCreds from '../../../components/profile/ProfileCreds';
import Spinner from '../../../components/common/Spinner';
import { getProfileByHandle } from '../../../actions/profileActions';
import { getVideos } from '../../../actions/videoActions';
import VideoFeed from '../../../components/profile/uservideos/VideoFeed';
import '../../../components/profile/uservideos/videos.css';

import { Tabs } from 'antd';

const { TabPane } = Tabs;

class Profile extends Component {
  state = { size: 'small' };

  onChange = (e) => {
    this.setState({ size: e.target.value });
  };
  componentDidMount() {
    if (this.props.match.params.handle) {
      this.props.getProfileByHandle(this.props.match.params.handle);
      this.props.getVideos();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.profile === null && this.props.profile.loading) {
      this.props.history.push('/not-found');
    }
  }

  render() {
    const { profile, loading } = this.props.profile;
    let profileContent;
    const { size } = this.state;
    const { videos } = this.props.video;
    let videoContent;
    if (videos === null || loading) {
      videoContent = <Spinner />;
    } else {
      videoContent = <VideoFeed videos={videos} />;
    }

    if (profile === null || loading) {
      profileContent = <Spinner />;
    } else {
      profileContent = (
        <div>
          <br />
          {/* <div className="row">
            <div className="col-md-6">
              <br />
              <Link to="/profiles" className="btn btn-light mb-3 float-left">
                Back To Profiles
              </Link>
            </div>
            <div className="col-md-6" />
          </div> */}
          <div className='row'>
            <div className='col-md-3'>
              <ProfileHeader profile={profile} />
            </div>
            <div className='col-md-8'>
              <Tabs defaultActiveKey='1' size={size}>
                <TabPane tab='Videos' key='1'>
                  <div className='feed'>
                    <div className='container'>
                      <div className='row'>{videoContent}</div>
                    </div>
                  </div>
                </TabPane>
                <TabPane tab='Profile' key='2'>
                  <ProfileAbout profile={profile} />
                  <ProfileCreds
                    education={profile.education}
                    experience={profile.experience}
                  />
                </TabPane>
              </Tabs>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className='profile'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-12'>{profileContent}</div>
            <br />
            <br />

            <br />

            <br />
          </div>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  getProfileByHandle: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  getVideos: PropTypes.func.isRequired,
  video: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
  video: state.video,
});

export default connect(
  mapStateToProps,
  { getProfileByHandle, getVideos }
)(Profile);
