import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Spinner from '../../../../components/common/Spinner';
import {
  getEntry,
  addLike,
  removeLike,
} from '../../../../actions/entryActions';
import './scss/video-react.scss'; // import css
import classnames from 'classnames';
import './entryItem.css';

import { Typography } from 'antd';

// import { Card } from "antd";
// import { Icon, Avatar, Comment, Tooltip, List } from "antd";

// const { Meta } = Card;
const { Text } = Typography;

class VideoEntry extends Component {
  componentDidMount() {
    this.props.getEntry(this.props.match.params.id);
  }
  onLikeClick(id) {
    this.props.addLike(id);
  }

  onUnlikeClick(id) {
    this.props.removeLike(id);
  }

  findUserLike(likes) {
    const { auth } = this.props;
    if (likes.filter((like) => like.user === auth.user.id).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    const { entry, loading } = this.props.entry;
    let entryContent;

    if (entry === null || loading || Object.keys(entry).length === 0) {
      entryContent = <Spinner />;
    } else {
      entryContent = (
        <div>
          <div className='heroUnit'>
            <br />
            <br />
            <div className='heroContentGenres'>
              <Typography
                component='h1'
                variant='h2'
                align='center'
                style={{ color: 'white' }}
              >
                {entry.title}
              </Typography>
              <Typography
                variant='h6'
                align='center'
                style={{ color: 'white' }}
              >
                {entry.description}
              </Typography>
              <br />
              <Typography variant='h6' align='center' style={{ color: 'pink' }}>
                Uploaded by: &nbsp;
                <Link to={`/profile/${entry.name}`}>
                  <Text strong style={{ color: 'pink' }}>
                    {entry.name}
                  </Text>
                </Link>
              </Typography>
            </div>
          </div>
          <div className='post row'>
            <div className='col-md-2' />
            <div className='videoplayer col-md-8'>
              {/* <Player>
                <source
                  src={require(`../../../compEntry/${entry.entrypath}`)}
                />
              </Player> */}
            </div>
            <div className='col-md-2' />
            <div className='col-md-5' />
            <div className='col-md-4'>
              <br />
              <h6>
                {/* (To like or Unlike this video click here) */}
                Like &nbsp;
                <button
                  onClick={this.onLikeClick.bind(this, entry._id)}
                  type='button'
                  className='btn btn-light mr-1'
                >
                  <i
                    className={classnames('fas fa-thumbs-up', {
                      'text-info': this.findUserLike(entry.likes),
                    })}
                  />
                  <span className='badge badge-light'>
                    {entry.likes.length}
                  </span>
                </button>
                &nbsp; &nbsp;Unlike &nbsp;
                <button
                  onClick={this.onUnlikeClick.bind(this, entry._id)}
                  type='button'
                  className='btn btn-light mr-1'
                >
                  <i className='text-secondary fas fa-thumbs-down' />
                </button>
              </h6>
              {/* <h4>
                <Text>Comments:</Text>
              </h4> */}
              <br />
              {/* <CommentForm entryId={entry._id} />
              <CommentFeed entryId={entry._id} comments={entry.comments} /> */}
            </div>

            {/* <div className="container">
              <div className="row">
                <div className="col-md-3">
                  <br />
                  <h6>
                    (To like or Unlike this video click here)
                    <button
                      onClick={this.onLikeClick.bind(this, video._id)}
                      type="button"
                      className="btn btn-light mr-1"
                    >
                      <i
                        className={classnames("fas fa-thumbs-up", {
                          "text-info": this.findUserLike(video.likes)
                        })}
                      />
                      <span className="badge badge-light">
                        {video.likes.length}
                      </span>
                    </button>
                    <button
                      onClick={this.onUnlikeClick.bind(this, video._id)}
                      type="button"
                      className="btn btn-light mr-1"
                    >
                      <i className="text-secondary fas fa-thumbs-down" />
                    </button>
                  </h6>
                  <h4>
                    <Text>Comments:</Text>
                  </h4>
                  <CommentForm videoId={video._id} />
                  <CommentFeed videoId={video._id} comments={video.comments} />
                </div>
              </div>
            </div> */}
          </div>
        </div>
      );
    }

    return (
      <div>
        {entryContent}
        {/* <div className="post">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <br />
                <Link to="/entrys2" className="btn btn-light mb-3">
                  Back to Discover
                </Link>
                {videoContent}
              </div>
            </div>
          </div>
        </div> */}
      </div>
    );
  }
}

VideoEntry.propTypes = {
  getEntry: PropTypes.func.isRequired,
  addLike: PropTypes.func.isRequired,
  removeLike: PropTypes.func.isRequired,
  entry: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  entry: state.entry,
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { getEntry, addLike, removeLike }
)(VideoEntry);
