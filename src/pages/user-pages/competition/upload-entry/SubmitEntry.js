import React, { Component } from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import "./stepOne.css";
import { Upload, Icon, message } from "antd";
//Brad
import PropTypes from "prop-types";
import { connect } from "react-redux";
import TextAreaFieldGroup from "../../../../components/common/TextAreaFieldGroup";
import TextFieldGroup from "../../../../components/common/TextFieldGroup";
import { addEntry } from "../../../../actions/entryActions";
import "antd/dist/antd.css";
// import { Link } from "react-router-dom";

export class SubmitEntry extends Component {
  //Brad

  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      entrypath: "",
      errors: {},
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const { user } = this.props.auth;
    const { competitionId } = this.props;

    const newEntry = {
      title: this.state.title,
      description: this.state.description,
      entrypath: this.state.entrypath,
      name: user.name,
      avatar: user.avatar,
    };
    this.props.addEntry(competitionId, newEntry);

    this.setState({ title: "", description: "", genre: "" });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;
    const Dragger = Upload.Dragger;
    const propsDragger = {
      name: "file",
      multiple: true,
      action: "http://localhost:5000/api/entries/uploadEntry",
      onChange(info) {
        const status = info.file.status;
        if (status !== "uploading") {
          console.log(info.file, info.fileList);
        }
        if (status === "done") {
          message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === "error") {
          message.error(`${info.file.name} file upload failed.`);
        }
      },
    };

    return (
      <MuiThemeProvider>
        <React.Fragment>
          <div>
            <div className="feed">
              <div className="container">
                {/* <div className="post-form mb-3">
                  <br />
                  <div className="card card-info"> */}
                <br />
                <div className="card-header bg-info text-white">
                  Upload your Entry to the Competition!
                </div>
                <div className="card-body">
                  <form onSubmit={this.onSubmit}>
                    <br />
                    <TextFieldGroup
                      placeholder="Title"
                      name="title"
                      value={this.state.title}
                      onChange={this.onChange}
                      error={errors.title}
                      info="Enter the Title for your video"
                    />
                    <TextAreaFieldGroup
                      placeholder="Description"
                      name="description"
                      value={this.state.description}
                      onChange={this.onChange}
                      error={errors.description}
                      info="Enter the Description for your video"
                    />
                    <Dragger {...propsDragger}>
                      <p className="ant-upload-drag-icon">
                        <Icon type="inbox" />
                      </p>
                      <p className="ant-upload-text">
                        Click or drag file to this area to upload
                      </p>
                      <p className="ant-upload-hint">
                        Please make sure you are located in Pakistan. This site
                        only supports Pakistani Content.
                      </p>
                    </Dragger>
                    <br />
                    <button type="submit" className="btn btn-dark button">
                      Submit
                    </button>
                  </form>
                </div>
              </div>
            </div>
            {/* </div>
            </div> */}
          </div>
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

// const styles = {
//   button: {
//     margin: 15
//   }
// };

SubmitEntry.propTypes = {
  addEntry: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  competitionId: PropTypes.string.isRequired,
  errors: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});
export default connect(
  mapStateToProps,
  { addEntry }
)(SubmitEntry);
