import React, { Component } from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
// import { Jumbotron, Label } from "reactstrap";
import "./stepOne.css";
import { Link, withRouter } from "react-router-dom";

export class Success extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <React.Fragment>
          <div className="post-form mb-3">
            <div className="card card-info">
              <div className="card-header bg-info text-white scHeaderFontSize">
                Thank you for your submission!
              </div>
              <div className="card-body">
                <p className="scFontSize">
                  Your Video has been uploaded , Go check your profile to watch
                  it.
                </p>

                <p className="scFontSize">
                  If you want to upload another video click on the button below
                </p>

                <br />
                <Link to="/UploadForm2" className="btn btn-light uploadbutton">
                  Upload
                </Link>
              </div>
            </div>
          </div>
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

export default Success;
