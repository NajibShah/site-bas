import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CommentFormComp from '../../../../components/competition/CommentFormComp';
import CommentFeedComp from '../../../../components/competition/CommentFeedComp';
import EntryFeed from '../../../../components/competition/entries/EntryFeed';
import Spinner from '../../../../components/common/Spinner';

import { getCompetition } from '../../../../actions/competitionActions';
import { getEntries } from '../../../../actions/entryActions';

import SubmitEntry from '../upload-entry/SubmitEntry';
import dateFormat from 'dateformat';

import { Link } from 'react-router-dom';
import './uploadtocompetition/stepOne.css';

import { Tabs } from 'antd';

import { Typography } from 'antd';
const { TabPane } = Tabs;

class Competition extends Component {
  componentDidMount() {
    this.props.getCompetition(this.props.match.params.id);
    this.props.getEntries(this.props.match.params.id);
  }
  state = { size: 'small' };

  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  render() {
    const { competition, loading } = this.props.competition;
    const { entries } = this.props.entry;
    let competitionContent;

    const { size } = this.state;
    var startDate = dateFormat(competition.from, 'dddd, mmmm dS, yyyy');
    var endDate = dateFormat(competition.to, 'dddd, mmmm dS, yyyy');

    if (
      competition === null ||
      loading ||
      Object.keys(competition).length === 0
    ) {
      competitionContent = <Spinner />;
    } else {
      competitionContent = (
        <div className='post row'>
          <br />
          {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
          <div className='col-md-4'>
            <Tabs tabPosition='left' defaultActiveKey='1' size={size}>
              <TabPane tab='Comments' key='1'>
                <br />
                <CommentFormComp competitionId={competition._id} />
                <br />
                <CommentFeedComp
                  competitionId={competition._id}
                  comments={competition.comments}
                />
              </TabPane>
            </Tabs>
          </div>
          <br />
          {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
          {/* <div className=" col-md-1"></div> */}
          <div className='col-md-8'>
            <Tabs
              tabPosition='right'
              defaultActiveKey='1'
              size={size}
              style={{ color: 'white' }}
            >
              <TabPane tab='Upload' key='1'>
                <SubmitEntry competitionId={competition._id} />
              </TabPane>
              <TabPane tab='Entries' key='2'>
                <div className='feed'>
                  <div className='container'>
                    <div className='row'>
                      <EntryFeed entries={entries} />
                    </div>
                  </div>
                </div>
              </TabPane>
            </Tabs>
          </div>
        </div>
      );
    }

    return (
      <div className='compitembg'>
        <div className='bg-transparent'>
          <br />
          <Link to='/competitionslanding' className='btn btn-light mb-3'>
            Back to Competitions
          </Link>

          <div className='heroContentGenres'>
            <Typography
              component='h1'
              variant='h2'
              align='center'
              style={{ color: 'white' }}
            >
              {competition.title}
            </Typography>
            <Typography
              component='h5'
              align='center'
              style={{
                color: 'white',
                paddingLeft: '40px',
                paddingRight: '40px',
              }}
            >
              {competition.description}
            </Typography>
            <br />
            <Typography variant='h6' align='center' style={{ color: 'pink' }}>
              Start Date: &nbsp;
              {startDate}
            </Typography>
            <Typography variant='h6' align='center' style={{ color: 'pink' }}>
              End Date: &nbsp;
              {endDate}
            </Typography>
            <hr style={{ borderTop: '2px dashed aqua' }} />
          </div>
        </div>
        <div className='competitionContent'>{competitionContent}</div>
      </div>
    );
  }
}

Competition.propTypes = {
  getCompetition: PropTypes.func.isRequired,
  getEntries: PropTypes.func.isRequired,
  competition: PropTypes.object.isRequired,
  entry: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  competition: state.competition,
  entry: state.entry,
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { getCompetition, getEntries }
)(Competition);
