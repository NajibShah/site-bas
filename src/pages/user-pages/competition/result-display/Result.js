import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ResultFeed from '../../../../components/competitions/result/ResultFeed';
import Spinner from '../../../../components/common/Spinner';

import { getCompetition } from '../../../../actions/competitionActions';
import { getEntriesLikes } from '../../../../actions/entryActions';

import dateFormat from 'dateformat';

import { Link } from 'react-router-dom';
import './stepOne.css';

import { Typography } from 'antd';

class Result extends Component {
  componentDidMount() {
    this.props.getCompetition(this.props.match.params.id);
    this.props.getEntriesLikes(this.props.match.params.id);
  }

  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  render() {
    const { competition, loading } = this.props.competition;
    const { entries } = this.props.entry;
    let competitionContent;
    let noResultMessage;

    var startDate = dateFormat(competition.from, 'dddd, mmmm dS, yyyy');
    var endDate = dateFormat(competition.to, 'dddd, mmmm dS, yyyy');

    if (entries === null || loading || Object.keys(entries).length === 0) {
      noResultMessage = (
        <div>
          <br />
          <br />
          <br />
          <Typography
            component='h1'
            variant='h2'
            align='center'
            style={{ color: 'white' }}
          >
            No Entries!
          </Typography>
          <Typography component='h5' align='center' style={{ color: 'pink' }}>
            Unfortunately there were no entries submitted to this competition
            ":("
          </Typography>
          <br />
          <br />
          <br />
        </div>
      );
    } else {
      noResultMessage = (
        <div>
          <br />
          <br />
          <Typography
            component='h1'
            variant='h2'
            align='center'
            style={{ color: 'white' }}
          >
            Top Entries
          </Typography>
          <Typography component='h5' align='center' style={{ color: 'pink' }}>
            Winners are based on most likes, and are sorted as such below.
          </Typography>
        </div>
      );
    }
    if (
      competition === null ||
      loading ||
      Object.keys(competition).length === 0
    ) {
      competitionContent = <Spinner />;
    } else {
      competitionContent = (
        <div className='post row'>
          <div className='col-md-12' style={{ textAlign: 'center' }}>
            <hr style={{ borderTop: '2px dashed black', width: '75%' }} />
            {noResultMessage}
            {/* <hr style={{ borderTop: "2px dashed aqua" }} /> */}
          </div>
          <ResultFeed entries={entries} />
          <br />
          <br />
          <br />
        </div>
      );
    }

    return (
      <div className='compitembg'>
        <div className='bg-transparent'>
          <br />
          <Link to='/resultslanding' className='btn btn-light mb-3'>
            Back to Results
          </Link>
          <div className='heroContentGenres'>
            <Typography
              component='h1'
              variant='h2'
              align='center'
              style={{ color: 'white' }}
            >
              {competition.title}
            </Typography>
            <div className='col-md-8 offset-md-2'>
              <Typography
                component='h5'
                align='center'
                style={{ color: 'white' }}
              >
                {competition.description}
              </Typography>
            </div>
            <br />
            <Typography variant='h6' align='center' style={{ color: 'pink' }}>
              Start Date: &nbsp;
              {startDate}
            </Typography>
            <Typography variant='h6' align='center' style={{ color: 'pink' }}>
              End Date: &nbsp;
              {endDate}
            </Typography>
          </div>
        </div>
        <div className='competitionContent'>{competitionContent}</div>
      </div>
    );
  }
}

Result.propTypes = {
  getCompetition: PropTypes.func.isRequired,
  getEntriesLikes: PropTypes.func.isRequired,
  competition: PropTypes.object.isRequired,
  entry: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  competition: state.competition,
  entry: state.entry,
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { getCompetition, getEntriesLikes }
)(Result);
