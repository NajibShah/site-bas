import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import CompFeed from "./active/CompFeed";
import ResFeed from '../../../../components/competitions/results/ResFeed';
import Spinner from '../../../../components/common/Spinner';
import { getCompetitions } from '../../../../actions/competitionActions';

import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

class ResLanding extends Component {
  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  componentDidMount() {
    this.props.getCompetitions();
  }

  render() {
    const { competitions, loading } = this.props.competition;
    let resultContent;

    if (competitions === null || loading) {
      resultContent = <Spinner />;
    } else {
      resultContent = <ResFeed competitions={competitions} />;
    }

    return (
      <div>
        <div className='heroUnit'>
          <div className='heroContent'>
            <Typography
              component='h1'
              variant='h3'
              align='center'
              style={{ color: 'white' }}
              gutterBottom
            >
              Results
            </Typography>
            <Typography
              variant='h8'
              align='center'
              style={{ color: 'pink' }}
              paragraph
            >
              This where you can view results of previous competitions
            </Typography>
            <br />
            <div className='heroButtons'>
              <Grid container spacing={16} justify='center'>
                <Grid item>
                  <Link to='/competitionslanding'>
                    <Button variant='outlined' style={{ color: 'orange' }}>
                      Back to competitions
                    </Button>
                  </Link>
                </Grid>
                {/* <Grid item>
                  <Link to="dicover/length">
                    <Button variant="outlined" color="primary">
                      By Length
                    </Button>
                  </Link>
                </Grid> */}
              </Grid>
            </div>
          </div>
        </div>
        <div className='feed'>
          <div className='container'>
            <div className='row'>{resultContent}</div>
          </div>
        </div>
      </div>
    );
  }
}

ResLanding.propTypes = {
  getCompetitions: PropTypes.func.isRequired,
  competition: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  competition: state.competition,
});

export default connect(
  mapStateToProps,
  { getCompetitions }
)(ResLanding);
