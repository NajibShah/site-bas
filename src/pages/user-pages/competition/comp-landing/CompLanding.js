import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import CompFeed from "../../../../components/competitions/active/CompFeed";
import ResFeed from "../../../../components/competitions/results/ResFeed";
import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

import Spinner from "../../../../components/common/Spinner";
import { getCompetitions } from "../../../../actions/competitionActions";

class CompLanding extends Component {
  state = { size: "small" };

  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  componentDidMount() {
    this.props.getCompetitions();
  }

  render() {
    const { competitions, loading } = this.props.competition;
    let competitionContent;
    let resultContent;

    if (competitions === null || loading) {
      competitionContent = <Spinner />;
    } else {
      competitionContent = <CompFeed competitions={competitions} />;
    }
    if (competitions === null || loading) {
      resultContent = <Spinner />;
    } else {
      resultContent = <ResFeed competitions={competitions} />;
    }

    return (
      <div>
        <div className="heroUnit">
          <div className="heroContent">
            <Typography
              component="h1"
              variant="h3"
              align="center"
              style={{ color: "white" }}
              gutterBottom
            >
              Festivals
            </Typography>
            <Typography
              variant="h8"
              align="center"
              style={{ color: "pink" }}
              paragraph
            >
              This where you can discover active and upcoming competitions{" "}
              <br />
            </Typography>
            <br />
            <div className="heroButtons">
              <Grid container spacing={16} justify="center">
                <Grid item>
                  <Link to="/resultslanding">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      View results
                    </Button>
                  </Link>
                </Grid>
                {/* <Grid item>
                  <Link to="dicover/length">
                    <Button variant="outlined" color="primary">
                      By Length
                    </Button>
                  </Link>
                </Grid> */}
              </Grid>
            </div>
          </div>
        </div>

        <div className="feed">
          <div className="container">
            <div className="row">{competitionContent}</div>
          </div>
        </div>
      </div>
    );
  }
}

CompLanding.propTypes = {
  getCompetitions: PropTypes.func.isRequired,
  competition: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  competition: state.competition,
});

export default connect(
  mapStateToProps,
  { getCompetitions }
)(CompLanding);
