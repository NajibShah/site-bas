import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

class AdminLanding extends Component {
  state = { size: 'small' };

  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  render() {
    return (
      <div>
        <div className='heroUnit'>
          <div className='heroContent'>
            <Typography
              component='h1'
              variant='h3'
              align='center'
              style={{ color: 'white' }}
              gutterBottom
            >
              Administration
            </Typography>
            <Typography
              variant='h8'
              align='center'
              style={{ color: 'pink' }}
              paragraph
            >
              This where you can perform all of the tasks listed below. To use
              any of the tools just click on of the buttons below.
            </Typography>
            <br />

            <div className='heroButtons'>
              <Grid container spacing={16} justify='center'>
                <Grid item>
                  <Link to='UploadForm1'>
                    <Button variant='outlined' style={{ color: 'pink' }}>
                      Upload Video
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to='uploadcompetition'>
                    <Button variant='outlined' style={{ color: 'pink' }}>
                      Upload competition
                    </Button>
                  </Link>
                </Grid>
                {/* Results were automated
                So it was removed from admin panel */}
                {/* <Grid item>
                  <Link to="postresult">
                    <Button variant="outlined" style={{ color: "pink" }}>
                      Post Result
                    </Button>
                  </Link>
                </Grid> */}
                <Grid item>
                  <Link to='deletevideo'>
                    <Button variant='outlined' style={{ color: 'pink' }}>
                      Delete Video
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to='deletecompetition'>
                    <Button variant='outlined' style={{ color: 'pink' }}>
                      Delete Competition
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to='deleteuser'>
                    <Button variant='outlined' style={{ color: 'pink' }}>
                      Delete User
                    </Button>
                  </Link>
                </Grid>

                {/* <Grid item>
                  <Link to="suspense">
                    <Button variant="outlined" color="primary">
                      Suspense{" "}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="suspense">
                    <Button variant="outlined" color="primary">
                      Romantic Comedy{" "}
                    </Button>
                  </Link>
                </Grid> */}
              </Grid>
            </div>
          </div>
        </div>
        <br />
        {/* <Tabs defaultActiveKey="1" size={size}>
          <TabPane tab="Start Competition" key="1">
            <br />
            <NewComp />
          </TabPane>
          <TabPane tab="Post Result" key="2">
            <br />
            <Link to="adminLogin">Click here to go to Post a result</Link>
            <br />
          </TabPane>
          <TabPane tab="Delete Video" key="3">
            <br />
            <Link to="adminLogin">Click here to go to Delete a video</Link>
            <br />
          </TabPane>
          <TabPane tab="Delete Competition" key="4">
            <br />
            <Link to="adminLogin">Click here to go to Delete a video</Link>
            <br />
          </TabPane>
          <TabPane tab="Delete Result" key="5">
            <br />
            <Link to="adminLogin">Click here to go to Delete a video</Link>
            <br />
          </TabPane>
          <TabPane tab="Delete User" key="6">
            <br />
            <Link to="adminLogin">Click here to go to Delete a video</Link>
            <br />
          </TabPane>
        </Tabs> */}
      </div>
    );
  }
}

export default AdminLanding;
