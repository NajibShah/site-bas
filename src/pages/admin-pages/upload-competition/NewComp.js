import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
// import { Jumbotron, Label } from "reactstrap";
// import "./stepOne.css";
//Brad
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextAreaFieldGroup from '../../../components/common/TextAreaFieldGroup';
import TextFieldGroup from '../../../components/common/TextFieldGroup';
import { addCompetition } from '../../../actions/competitionActions';
// import { CLEAR_ERRORS } from "../../actions/types";
import 'antd/dist/antd.css';
// import { Redirect } from "react-router";

import Typography from '@material-ui/core/Typography';

class NewComp extends Component {
  //Brad
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      from: '',
      to: '',
      current: false,
      imagepath: '',
      errors: {},
      redirect: false,
      disabled: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onCheck = this.onCheck.bind(this);
  }
  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const { user } = this.props.auth;

    const newCompetition = {
      title: this.state.title,
      description: this.state.description,
      name: user.name,
      from: this.state.from,
      to: this.state.to,
      current: this.state.current,
      avatar: user.avatar,
    };
    this.props.addCompetition(newCompetition);

    this.setState({
      title: '',
      description: '',
      from: '',
      to: '',
      current: !this.state.current,
      disabled: !this.state.disabled,
      redirect: true,
    });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onCheck(e) {
    this.setState({
      disabled: !this.state.disabled,
      current: !this.state.current,
      from: Date(),
    });
  }

  render() {
    const { errors } = this.state;

    return (
      // <MuiThemeProvider>
      //   <React.Fragment>
      <div>
        <div className='heroUnit'>
          <div className='row'>
            <div className='col-md-6'>
              <br />
              <Link
                to='/adminlanding'
                className='btn btn-light mb-3 float-left'
                style={{ color: 'gray' }}
              >
                Back To Administration
              </Link>
            </div>
            <div className='col-md-6' />
          </div>
          <div className='heroContentGenres'>
            <Typography
              component='h1'
              variant='h3'
              align='center'
              style={{ color: 'white' }}
              gutterBottom
            >
              New Competition
            </Typography>
            <Typography
              variant='h8'
              align='center'
              style={{ color: 'pink' }}
              paragraph
            >
              This where you can upload a new competition to Filmabad!
            </Typography>
          </div>
        </div>
        <div className='post-form mb-3'>
          <div className='feed'>
            <div className='container'>
              <div className='card card-info'>
                <div className='card-header bg-info text-white'>
                  Upload a Competition to Filmabad!
                </div>
                <div className='card-body'>
                  <form onSubmit={this.onSubmit}>
                    <br />
                    <TextFieldGroup
                      placeholder='Title'
                      name='title'
                      value={this.state.title}
                      onChange={this.onChange}
                      error={errors.title}
                      info='Enter the Title for your Competition'
                    />
                    <TextAreaFieldGroup
                      placeholder='Description'
                      name='description'
                      value={this.state.description}
                      onChange={this.onChange}
                      error={errors.description}
                      info='Enter the Description for your Competition'
                    />
                    <br />
                    <h6>From Date</h6>
                    <TextFieldGroup
                      name='from'
                      type='date'
                      value={this.state.from}
                      onChange={this.onChange}
                      error={errors.from}
                      disabled={this.state.disabled ? 'disabled' : ''}
                    />
                    <div className='form-check mb-4'>
                      <input
                        type='checkbox'
                        className='form-check-input'
                        name='current'
                        value={this.state.current}
                        checked={this.state.current}
                        onChange={this.onCheck}
                        id='current'
                      />
                      <label htmlFor='current' className='form-check-label'>
                        Current Date
                      </label>
                    </div>
                    <h6>To Date</h6>
                    <TextFieldGroup
                      name='to'
                      type='date'
                      value={this.state.to}
                      onChange={this.onChange}
                      error={errors.to}
                    />
                    <br />
                    {/* <Dragger {...propsDragger}>
                      <p className="ant-upload-drag-icon">
                        <Icon type="inbox" />
                      </p>
                      <p className="ant-upload-text">
                        Click or drag a file into this area to upload
                      </p>
                      <p className="ant-upload-hint">
                        This site only supports Pakistani Content.
                      </p>
                    </Dragger> */}
                    <br />
                    <button type='submit' className='btn btn-dark button'>
                      Submit
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      //* </React.Fragment>
      // </MuiThemeProvider>
    );
  }
}

NewComp.propTypes = {
  addCompetition: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(
  mapStateToProps,
  { addCompetition }
)(NewComp);
