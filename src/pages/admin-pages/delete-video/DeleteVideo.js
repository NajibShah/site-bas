import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import VideoFeed from '../../../components/admin/videos/VideoFeed';

import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';

import Spinner from '../../../components/common/Spinner';
import { getVideos } from '../../../actions/videoActions';
import './videos.css';

class DeleteVideo extends Component {
  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  componentDidMount() {
    this.props.getVideos();
  }

  render() {
    const { videos, loading } = this.props.video;
    let videoContent;

    if (videos === null || loading) {
      videoContent = <Spinner />;
    } else {
      videoContent = <VideoFeed videos={videos} />;
    }

    return (
      <div>
        <div className='heroUnit'>
          <div className='row'>
            <div className='col-md-6'>
              <br />
              <Link
                to='/adminlanding'
                className='btn btn-light mb-3 float-left'
                style={{ color: 'gray' }}
              >
                Back To Administration
              </Link>
            </div>
            <div className='col-md-6' />
          </div>
          <div className='heroContentGenres'>
            <Typography
              component='h1'
              variant='h3'
              align='center'
              style={{ color: 'white' }}
              gutterBottom
            >
              Delete Video
            </Typography>
            <Typography
              variant='h8'
              align='center'
              style={{ color: 'pink' }}
              paragraph
            >
              This where you can delete a video from Filmabad!
            </Typography>
          </div>
        </div>
        <br />

        <div className='feed'>
          <div className='container'>
            <div className='row'>{videoContent}</div>
          </div>
        </div>
      </div>
    );
  }
}

DeleteVideo.propTypes = {
  getVideos: PropTypes.func.isRequired,
  video: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  video: state.video,
});

export default connect(
  mapStateToProps,
  { getVideos }
)(DeleteVideo);
