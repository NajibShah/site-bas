import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import CompFeed from "../../../components/admin/competitions/CompFeed";

import { Link } from "react-router-dom";

import Spinner from "../../../components/common/Spinner";
import { getCompetitions } from "../../../actions/competitionActions";

import Typography from "@material-ui/core/Typography";

class CompLanding extends Component {
  state = { size: "small" };

  onChange = (e) => {
    this.setState({ size: e.target.value });
  };

  componentDidMount() {
    this.props.getCompetitions();
  }

  render() {
    const { competitions, loading } = this.props.competition;
    let competitionContent;

    if (competitions === null || loading) {
      competitionContent = <Spinner />;
    } else {
      competitionContent = <CompFeed competitions={competitions} />;
    }

    return (
      <div>
        <div className="heroUnit">
          <div className="row">
            <div className="col-md-6">
              <br />
              <Link
                to="/adminlanding"
                className="btn btn-light mb-3 float-left"
                style={{ color: "gray" }}
              >
                Back To Administration
              </Link>
            </div>
            <div className="col-md-6" />
          </div>
          <div className="heroContentGenres">
            <Typography
              component="h1"
              variant="h3"
              align="center"
              style={{ color: "white" }}
              gutterBottom
            >
              Delete Competition
            </Typography>
            <Typography
              variant="h8"
              align="center"
              style={{ color: "pink" }}
              paragraph
            >
              This where you can delete an existing or expired competition from
              Filmabad!{" "}
            </Typography>
          </div>
        </div>
        <div className="feed">
          <div className="container">
            <div className="row">{competitionContent}</div>
          </div>
        </div>
      </div>
    );
  }
}

CompLanding.propTypes = {
  getCompetitions: PropTypes.func.isRequired,
  competition: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  competition: state.competition,
});

export default connect(
  mapStateToProps,
  { getCompetitions }
)(CompLanding);
