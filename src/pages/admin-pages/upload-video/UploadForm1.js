import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Label } from 'reactstrap';
import './stepOne.css';
import { Upload, Icon, message } from 'antd';
//Brad
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextAreaFieldGroup from '../../../components/common/TextAreaFieldGroup';
import TextFieldGroup from '../../../components/common/TextFieldGroup';
import { addVideo } from '../../../actions/videoActions';
import 'antd/dist/antd.css';
import { Link } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';

export class UploadForm1 extends Component {
  //Brad

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      genre: '',
      videopath: '',
      errors: {},
      redirect: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const { user } = this.props.auth;
    const newVideo = {
      title: this.state.title,
      description: this.state.description,
      videopath: this.state.videopath,
      genre: this.state.genre,
      name: user.name,
      avatar: user.avatar,
    };
    this.props.addVideo(newVideo);
    if (this.state.title && this.state.description && this.state.genre) {
      this.props.history.push('/success');
    }
    this.setState({ title: '', description: '', genre: '', redirect: true });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;
    const Dragger = Upload.Dragger;
    const propsDragger = {
      name: 'file',
      multiple: true,
      action: 'http://localhost:5000/api/videos/upload',
      onChange(info) {
        const status = info.file.status;
        if (status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (status === 'done') {
          message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
      },
    };

    return (
      <MuiThemeProvider>
        <React.Fragment>
          <div>
            <div className='heroUnit'>
              <div className='row'>
                <div className='col-md-6'>
                  <br />
                  <Link
                    to='/adminlanding'
                    className='btn btn-light mb-3 float-left'
                    style={{ color: 'gray' }}
                  >
                    Back To Administration
                  </Link>
                </div>
                <div className='col-md-6' />
              </div>
              <div className='heroContentGenres'>
                <Typography
                  component='h1'
                  variant='h3'
                  align='center'
                  style={{ color: 'white' }}
                  gutterBottom
                >
                  Upload Page
                </Typography>
                <Typography
                  variant='h'
                  align='center'
                  style={{ color: 'pink' }}
                  paragraph
                >
                  Here is where you can Upload your video, a film, a music
                  video, and even a documentary to Filmabad!
                </Typography>
              </div>
            </div>
            <div className='feed'>
              <div className='container'>
                <br />
                {/* <div className="post-form mb-3">
                  <br />
                  <div className="card card-info"> */}
                <div className='card-header bg-info text-white'>
                  Upload your video to Filmabad!
                </div>
                <div className='card-body'>
                  <form onSubmit={this.onSubmit}>
                    <br />
                    <TextFieldGroup
                      placeholder='Title'
                      name='title'
                      value={this.state.title}
                      onChange={this.onChange}
                      error={errors.title}
                      info='Enter the Title for your video'
                    />
                    <TextAreaFieldGroup
                      placeholder='Description'
                      name='description'
                      value={this.state.description}
                      onChange={this.onChange}
                      error={errors.description}
                      info='Enter the Description for your video'
                    />
                    <Label className='col-md-offset-2 col-sm-offset-2 col-md-2 col-sm-2 label'>
                      Genre:
                    </Label>
                    <select
                      className='inputStyles drop col-md-2 col-sm-2'
                      type='text'
                      name='genre'
                      placeholder='Genre'
                      value={this.state.genre}
                      onChange={this.onChange}
                      error={errors.genre}
                    >
                      <option>Select A Genre</option>
                      <option>Horror</option>
                      <option>Comedy</option>
                      <option>Sci-Fi</option>
                      <option>Documentary</option>
                      <option>Thriller</option>
                      <option>Suspense</option>
                      <option>Romantic Comedy</option>
                    </select>
                    <Dragger {...propsDragger}>
                      <p className='ant-upload-drag-icon'>
                        <Icon type='inbox' />
                      </p>
                      <p className='ant-upload-text'>
                        Click or drag file to this area to upload
                      </p>
                      <p className='ant-upload-hint'>
                        Please make sure you are located in Pakistan. This site
                        only supports Pakistani Content.
                      </p>
                    </Dragger>
                    <br />
                    <button type='submit' className='btn btn-dark button'>
                      Submit
                    </button>
                  </form>
                </div>
              </div>
            </div>
            {/* </div>
            </div> */}
          </div>
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

UploadForm1.propTypes = {
  addVideo: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});
export default connect(
  mapStateToProps,
  { addVideo }
)(UploadForm1);
