import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "../../../components/common/Spinner";
import ProfileItem from "../../../components/admin/users/ProfileItem";
import { getProfiles } from "../../../actions/profileActions";
import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";

class DeleteUser extends Component {
  componentDidMount() {
    this.props.getProfiles();
  }

  render() {
    const { profiles, loading } = this.props.profile;
    let profileItems;

    if (profiles === null || loading) {
      profileItems = <Spinner />;
    } else {
      if (profiles.length > 0) {
        profileItems = profiles.map((profile) => (
          <ProfileItem key={profile._id} profile={profile} />
        ));
      } else {
        profileItems = <h4>No profiles found...</h4>;
      }
    }

    return (
      <div>
        <div className="heroUnit">
          <div className="row">
            <div className="col-md-6">
              <br />
              <Link
                to="/adminlanding"
                className="btn btn-light mb-3 float-left"
                style={{ color: "gray" }}
              >
                Back To Administration
              </Link>
            </div>
            <div className="col-md-6" />
          </div>
          <div className="heroContentGenres">
            <Typography
              component="h1"
              variant="h3"
              align="center"
              style={{ color: "white" }}
              gutterBottom
            >
              Delete User
            </Typography>
            <Typography
              variant="h8"
              align="center"
              style={{ color: "pink" }}
              paragraph
            >
              This where you can delete a user from Filmabad!
            </Typography>
          </div>
        </div>
        <div className="profiles">
          <div className="container">
            <div className="row">
              <div className="col-md-12">{profileItems}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DeleteUser.propTypes = {
  getProfiles: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(
  mapStateToProps,
  { getProfiles }
)(DeleteUser);
