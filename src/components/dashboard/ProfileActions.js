import React from "react";
import { Link } from "react-router-dom";

import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const ProfileActions = () => {
  return (
    <div>
      <div className="heroButtons">
        <Grid container spacing={16} justify="center">
          <Grid item>
            <Link to="/add-experience">
              <Button variant="outlined" style={{ color: "pink" }}>
                <i className="fab fa-black-tie text-info mr-1" />
                Add Experience
              </Button>
            </Link>
          </Grid>
          <Grid item>
            <Link to="/edit-profile">
              <Button variant="outlined" style={{ color: "pink" }}>
                <i className="fas fa-user-circle text-info mr-1" /> Edit Profile
              </Button>
            </Link>
          </Grid>
          <Grid item>
            <Link to="/add-experience">
              <Button variant="outlined" style={{ color: "pink" }}>
                <i className="fab fa-black-tie text-info mr-1" />
                Add Experience
              </Button>
            </Link>
          </Grid>
          <Grid item>
            <Link to="/add-education">
              <Button variant="outlined" style={{ color: "pink" }}>
                <i className="fas fa-graduation-cap text-info mr-1" />
                Add Education
              </Button>
            </Link>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default ProfileActions;
