import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteCompetition } from '../../../actions/competitionActions';
import './comps.css';
import { Link } from 'react-router-dom';

import { Card, Avatar } from 'antd';
// import {  Icon} from "antd";

const { Meta } = Card;

class CompItem extends Component {
  onDeleteClick(id) {
    this.props.deleteCompetition(id);
  }
  render() {
    const { competition } = this.props;
    let competitionsContent;
    const resDate = new Date(competition.to);
    const todayDate = new Date();

    console.log(resDate);
    console.log(todayDate);
    if (resDate > todayDate) {
      competitionsContent = (
        <Link to={`/competitions/${competition._id}`}>
          <Card
            hoverable
            className='card'
            style={{ width: 1100 }}
            // cover={
            //   <img
            //     alt="example"
            //     src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
            //   />
            // }
            // actions={[
            //   <Icon type="setting" key="setting" />,
            //   <Icon type="edit" key="edit" />,
            //   <Icon type="ellipsis" key="ellipsis" />
            // ]}
          >
            <Meta
              className='noOverflow'
              avatar={<Avatar src={competition.avatar} />}
              title={competition.title}
              description={competition.description}
            />
          </Card>
        </Link>
      );
    }
    return (
      <React.Fragment>
        <div>{competitionsContent}</div>

        {/* <Link to={`/competitions/${competition._id}`}>
          <Card
            hoverable
            className="card"
            style={{ width: 1100 }}
            // cover={
            //   <img
            //     alt="example"
            //     src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
            //   />
            // }
            // actions={[
            //   <Icon type="setting" key="setting" />,
            //   <Icon type="edit" key="edit" />,
            //   <Icon type="ellipsis" key="ellipsis" />
            // ]}
          >
            <Meta
              avatar={<Avatar src={competition.avatar} />}
              title={competition.title}
              description={competition.description}
            />
          </Card>
        </Link> */}
      </React.Fragment>

      // <div className="card card-body mb-3">
      //   <div className="row">
      //     <div className="col-md-2">
      //       <a href="profile.html">
      //         <img
      //           className="rounded-circle d-none d-md-block"
      //           src={video.avatar}
      //           alt=""
      //         />
      //       </a>
      //       <br />
      //       <p className="text-center">{video.name}</p>
      //     </div>
      //     <div className="col-md-10">
      //       <p className="lead">{video.title}</p>
      //       <p className="lead">{video.description}</p>

      //       <span>
      //         <button
      //           // onClick={this.onLikeClick.bind(this, video._id)}
      //           type="button"
      //           className="btn btn-light mr-1"
      //         >
      //           <i
      //             className={classnames("fas fa-thumbs-up", {
      //               // "text-info": this.findUserLike(video.likes)
      //             })}
      //           />
      //           <span className="badge badge-light">{video.likes.length}</span>
      //         </button>
      //         <button
      //           // onClick={this.onUnlikeClick.bind(this, video._id)}
      //           type="button"
      //           className="btn btn-light mr-1"
      //         >
      //           <i className="text-secondary fas fa-thumbs-down" />
      //         </button>
      //         <Link to={`/video/${video._id}`} className="btn btn-info mr-1">
      //           Comments
      //         </Link>
      //         {video.user === auth.user.id ? (
      //           <button
      //             onClick={this.onDeleteClick.bind(this, video._id)}
      //             type="button"
      //             className="btn btn-danger mr-1"
      //           >
      //             <i className="fas fa-times" />
      //           </button>
      //         ) : null}
      //       </span>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

CompItem.propTypes = {
  deleteCompetition: PropTypes.func.isRequired,
  competition: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { deleteCompetition }
)(CompItem);
