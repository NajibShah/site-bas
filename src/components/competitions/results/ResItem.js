import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteCompetition } from '../../../actions/competitionActions';
import './res.css';
import { Link } from 'react-router-dom';

import { Card, Avatar } from 'antd';
// import { Icon } from "antd";

const { Meta } = Card;

class ResItem extends Component {
  onDeleteClick(id) {
    this.props.deleteCompetition(id);
  }
  render() {
    const { competition } = this.props;
    let resultContent;
    const resDate = new Date(competition.to);
    const todayDate = new Date();

    console.log(resDate);
    console.log(todayDate);
    if (resDate < todayDate) {
      resultContent = (
        <Link to={`/results/${competition._id}`}>
          <Card
            className='card'
            style={{ width: 300 }}
            cover={
              <img
                alt='example'
                src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png'
              />
            }
            // actions={[
            //   <Icon type="setting" key="setting" />,
            //   <Icon type="edit" key="edit" />,
            //   <Icon type="ellipsis" key="ellipsis" />
            // ]}
          >
            <Meta
              className='noOverflow'
              avatar={<Avatar src={competition.avatar} />}
              title={competition.title}
              description={competition.description}
            />
          </Card>
        </Link>
      );
    }

    return (
      <React.Fragment>
        <div>{resultContent}</div>
        {/* <Link to={`/competition/${competition._id}`}>
          <Card
            className="card"
            style={{ width: 300 }}
            cover={
              <img
                alt="example"
                src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
              />
            }
            // actions={[
            //   <Icon type="setting" key="setting" />,
            //   <Icon type="edit" key="edit" />,
            //   <Icon type="ellipsis" key="ellipsis" />
            // ]}
          >
            <Meta
              avatar={<Avatar src={competition.avatar} />}
              title={competition.title}
              description={competition.description}
            />
          </Card>
        </Link> */}
      </React.Fragment>
    );
  }
}

ResItem.propTypes = {
  deleteCompetition: PropTypes.func.isRequired,
  competition: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { deleteCompetition }
)(ResItem);
