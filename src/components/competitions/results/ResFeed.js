import React, { Component } from "react";
import PropTypes from "prop-types";
import ResItem from "./ResItem";

class ResFeed extends Component {
  render() {
    const { competitions } = this.props;
    return competitions.map(competition => (
      <ResItem key={competition._id} competition={competition} />
    ));
  }
}

ResFeed.propTypes = {
  competitions: PropTypes.array.isRequired
};

export default ResFeed;
