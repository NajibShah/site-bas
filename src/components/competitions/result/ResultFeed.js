import React, { Component } from "react";
import PropTypes from "prop-types";
import ResultItem from "./ResultItem";

export class ResultFeed extends Component {
  render() {
    const { entries, competitionId } = this.props;
    return entries.map(entry => (
      <ResultItem key={entry._id} entry={entry} competitionId={competitionId} />
    ));
  }
}

ResultFeed.propTypes = {
  entries: PropTypes.array.isRequired,
  competitionId: PropTypes.string.isRequired
};

export default ResultFeed;
