import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './results.css';
import { Link } from 'react-router-dom';
import { deleteEntry } from '../../../actions/entryActions';

import { Typography } from 'antd';
const { Text } = Typography;

class ResultItem extends Component {
  onDeleteClick(id) {
    this.props.deleteEntry(id);
  }
  render() {
    const { entry } = this.props;

    return (
      <React.Fragment>
        <div className='col-md-12' style={{}}>
          <br />
          <br />
          <br />
          <div className='videoplayer col-md-8 offset-md-2 ' style={{}}>
            {/* <Player>
              <source src={require(`../../../compEntry/${entry.entrypath}`)} />
            </Player> */}
          </div>
          <div className='col-md-6 offset-md-2'>
            <Typography
              component='h1'
              variant='h2'
              align='left'
              style={{ color: 'white', marginTop: '2em' }}
            >
              {entry.title}
            </Typography>
            <Typography variant='h6' align='left' style={{ color: 'white' }}>
              {entry.description}
            </Typography>
            <br />
            <Typography variant='h6' align='left' style={{ color: 'pink' }}>
              Uploaded by: &nbsp;
              <Link to={`/profile/${entry.name}`}>
                <Text strong style={{ color: 'pink' }}>
                  {entry.name}
                </Text>
              </Link>
            </Typography>
          </div>
          <hr
            style={{
              borderTop: '1px dashed black',
              width: '50%',
              marginTop: '6em',
            }}
          />
        </div>
        {/* <Card
          className="card"
          style={{ width: 300 }}
          cover={
            <Link to={`/entries/entry/${entry._id}`}>
              <VideoThumbnail
                videoUrl={require(`../../../compEntry/${entry.entrypath}`)}
                thumbnailHandler={thumbnail => console.log(thumbnail)}
              />
            </Link>
          }
          actions={
            entry.user === auth.user.id || auth.user.name === "Filmabad"
              ? [
                  <Icon
                    type="delete"
                    key="delete"
                    onClick={this.onDeleteClick.bind(this, entry._id)}
                  />
                ]
              : null
          }
        >
          <Meta
            avatar={<Avatar src={entry.avatar} />}
            title={entry.title}
            description={entry.description}
          />
        </Card> */}
      </React.Fragment>
    );
  }
}

ResultItem.propTypes = {
  deleteEntry: PropTypes.func.isRequired,
  entry: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  profile: state.profile,
});

export default connect(
  mapStateToProps,
  { deleteEntry }
)(ResultItem);
