import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteVideo } from '../../../actions/videoActions';
import './videos.css';
import { Link } from 'react-router-dom';

import { Card, Icon, Avatar } from 'antd';

const { Meta } = Card;

class VideoItem extends Component {
  onDeleteClick(id) {
    this.props.deleteVideo(id);
  }
  render() {
    const { video } = this.props;
    const { profile } = this.props.profile;

    return (
      <React.Fragment>
        {video.name === profile.user.name
          ? [
              <Card
                className='card'
                style={{ width: 300 }}
                cover={
                  <Link to={`/video/${video._id}`}>
                    <img
                      alt='example'
                      src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png'
                    />
                  </Link>
                }
                actions={
                  // video.name === profile.handle
                  //   ? [
                  <Icon
                    type='delete'
                    key='delete'
                    // onClick={this.onDeleteClick.bind(this, video._id)}
                  />
                  // <Icon type="edit" key="edit" />,
                  // <Icon type="ellipsis" key="ellipsis" />
                  //   ]
                  // : null
                }
              >
                <Link to={`/video/${video._id}`}>
                  <Meta
                    className='noOverflow'
                    avatar={<Avatar src={video.avatar} />}
                    title={video.title}
                    description={video.description}
                  />
                </Link>
              </Card>,
            ]
          : null}
        {/* <Card
          className="card"
          style={{ width: 300 }}
          cover={
            <Link to={`/video/${video._id}`}>
              <img
                alt="example"
                src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
              />
            </Link>
          }
          actions={
            // video.name === profile.handle
            //   ? [
            <Icon
              type="delete"
              key="delete"
              // onClick={this.onDeleteClick.bind(this, video._id)}
            />
            // <Icon type="edit" key="edit" />,
            // <Icon type="ellipsis" key="ellipsis" />
            //   ]
            // : null
          }
        >
          <Link to={`/video/${video._id}`}>
            <Meta
              avatar={<Avatar src={video.avatar} />}
              title={video.title}
              description={video.description}
            />
          </Link>
        </Card> */}
      </React.Fragment>

      // <div className="card card-body mb-3">
      //   <div className="row">
      //     <div className="col-md-2">
      //       <a href="profile.html">
      //         <img
      //           className="rounded-circle d-none d-md-block"
      //           src={video.avatar}
      //           alt=""
      //         />
      //       </a>
      //       <br />
      //       <p className="text-center">{video.name}</p>
      //     </div>
      //     <div className="col-md-10">
      //       <p className="lead">{video.title}</p>
      //       <p className="lead">{video.description}</p>

      //       <span>
      //         <button
      //           // onClick={this.onLikeClick.bind(this, video._id)}
      //           type="button"
      //           className="btn btn-light mr-1"
      //         >
      //           <i
      //             className={classnames("fas fa-thumbs-up", {
      //               // "text-info": this.findUserLike(video.likes)
      //             })}
      //           />
      //           <span className="badge badge-light">{video.likes.length}</span>
      //         </button>
      //         <button
      //           // onClick={this.onUnlikeClick.bind(this, video._id)}
      //           type="button"
      //           className="btn btn-light mr-1"
      //         >
      //           <i className="text-secondary fas fa-thumbs-down" />
      //         </button>
      //         <Link to={`/video/${video._id}`} className="btn btn-info mr-1">
      //           Comments
      //         </Link>
      //         {video.user === auth.user.id ? (
      //           <button
      //             onClick={this.onDeleteClick.bind(this, video._id)}
      //             type="button"
      //             className="btn btn-danger mr-1"
      //           >
      //             <i className="fas fa-times" />
      //           </button>
      //         ) : null}
      //       </span>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

VideoItem.propTypes = {
  deleteVideo: PropTypes.func.isRequired,
  video: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  profile: state.profile,
});

export default connect(
  mapStateToProps,
  { deleteVideo }
)(VideoItem);
