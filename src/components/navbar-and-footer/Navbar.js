import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';
import { clearCurrentProfile } from '../../actions/profileActions';

class Navbar extends Component {
  onLogoutClick = (e) => {
    e.preventDefault();
    this.props.clearCurrentProfile();
    this.props.logoutUser();
  };

  render() {
    const { isAuthenticated, user } = this.props.auth;
    var authLinks = '';
    if (user.name === 'Filmabad') {
      authLinks = (
        <ul className='navbar-nav ml-auto'>
          <li className='nav-item'>
            <Link className='nav-link text-secondary' to='/adminLanding'>
              Admin Tools{' '}
            </Link>
          </li>
          {/* <li className="nav-item">
            <Link className="nav-link" to="/UploadForm2">
              Upload Video
            </Link>
          </li> */}
          <li className='nav-item'>
            <Link className='nav-link text-secondary' to='/competitionslanding'>
              Competitions
            </Link>
          </li>
          <li className='nav-item'>
            <Link className='nav-link text-secondary' to='/videos2'>
              Discover
            </Link>
          </li>
          {/* <li className="nav-item">
            <Link className="nav-link" to="/feed">
              Post Feed
            </Link>
          </li> */}
          <li className='nav-item'>
            <Link className='nav-link text-secondary' to='/dashboard'>
              Dashboard
            </Link>
          </li>
          <li className='nav-item'>
            <a
              href=' '
              onClick={this.onLogoutClick}
              className='nav-link text-info'
            >
              <img
                className='rounded-circle'
                src={user.avatar}
                alt={user.name}
                style={{ width: '25px', marginRight: '5px' }}
                title='You must have a Gravatar connected to your email to display an image'
              />{' '}
              Logout
            </a>
          </li>
        </ul>
      );
    } else {
      authLinks = (
        <ul className='navbar-nav ml-auto'>
          <li className='nav-item'>
            <Link className='nav-link text-secondary' to='/UploadForm2'>
              Upload Video
            </Link>
          </li>
          <li className='nav-item'>
            <Link className='nav-link text-secondary' to='/competitionslanding'>
              Competitions
            </Link>
          </li>
          {/* <li className="nav-item">
            <Link className="nav-link" to="/Videos">
              Discover
            </Link>
          </li> */}
          <li className='nav-item'>
            <Link className='nav-link text-secondary' to='/videos2'>
              Discover
            </Link>
          </li>
          {/* <li className="nav-item">
            <Link className="nav-link" to="/feed">
              Post Feed
            </Link>
          </li> */}
          <li className='nav-item'>
            <Link className='nav-link text-secondary' to='/dashboard'>
              Dashboard
            </Link>
          </li>
          <li className='nav-item'>
            <a
              href=' '
              onClick={this.onLogoutClick}
              className='nav-link text-info'
            >
              <img
                className='rounded-circle'
                src={user.avatar}
                alt={user.name}
                style={{ width: '25px', marginRight: '5px' }}
                title='You must have a Gravatar connected to your email to display an image'
              />{' '}
              Logout
            </a>
          </li>
        </ul>
      );
    }

    const guestLinks = (
      <ul className='navbar-nav ml-auto'>
        <li className='nav-item'>
          <Link className='nav-link text-info' to='/register'>
            Sign Up
          </Link>
        </li>
        <li className='nav-item'>
          <Link className='nav-link text-info' to='/login'>
            Login
          </Link>
        </li>
      </ul>
    );

    return (
      <nav className='navbar navbar-expand-sm navbar-dark bg-transparent mb-4'>
        <div className='container'>
          <Link className='navbar-brand text-dark' to='/'>
            BookAShaadi
          </Link>
          <button
            className='navbar-toggler'
            type='button'
            data-toggle='collapse'
            data-target='#mobile-nav'
          >
            <span
              className='navbar-toggler-icon'
              style={{ backgroundColor: 'silver' }}
            />
          </button>

          <div className='collapse navbar-collapse' id='mobile-nav'>
            <ul className='navbar-nav mr-auto'>
              <li className='nav-item'>
                <Link className='nav-link text-secondary' to='/profiles'>
                  {' '}
                  Creators
                </Link>
              </li>
              <li className='nav-item'>
                <Link className='nav-link text-secondary' to='/feed'>
                  {' '}
                  Blog
                </Link>
              </li>
            </ul>
            {isAuthenticated ? authLinks : guestLinks}
          </div>
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { logoutUser, clearCurrentProfile }
)(Navbar);
