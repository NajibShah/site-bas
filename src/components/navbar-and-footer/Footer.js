import React from "react";

export default () => {
  return (
    <footer className="bg-white mt-0 p-1 text-center">
      <p className="text-info">
        Copyright &copy; {new Date().getFullYear()} Filmabad. Made with love in
        Islamabad. @devBy NajibShah.
      </p>
    </footer>
  );
};
