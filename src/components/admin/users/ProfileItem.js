import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from '../../../validation/is-empty';
import { deleteAccountAdmin } from '../../../actions/profileActions';
import { connect } from 'react-redux';

class ProfileItem extends Component {
  onDeleteClick(id) {
    this.props.deleteAccountAdmin(id);
  }
  render() {
    const { profile } = this.props;

    return (
      <div className='card card-body bg-light mb-3'>
        <div className='row'>
          <div className='col-2'>
            <img src={profile.user.avatar} alt='' className='rounded-circle' />
          </div>
          <div className='col-lg-6 col-md-4 col-8'>
            <h3>{profile.user.name}</h3>
            <p>
              {profile.status}{' '}
              {isEmpty(profile.company) ? null : (
                <span>at {profile.company}</span>
              )}
            </p>
            <p>
              {isEmpty(profile.location) ? null : (
                <span>{profile.location}</span>
              )}
            </p>
            <button
              onClick={this.onDeleteClick.bind(this, profile.user._id)}
              className='btn btn-danger'
            >
              Delete Account
            </button>
          </div>
          <div className='col-md-4 d-none d-md-block'>
            <h4>Skill Set</h4>
            <ul className='list-group'>
              {profile.skills.slice(0, 4).map((skill, index) => (
                <li key={index} className='list-group-item'>
                  <i className='fa fa-check pr-1' />
                  {skill}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

ProfileItem.propTypes = {
  profile: PropTypes.object.isRequired,
  deleteAccountAdmin: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { deleteAccountAdmin }
)(ProfileItem);
