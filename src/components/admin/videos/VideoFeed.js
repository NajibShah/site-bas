import React, { Component } from "react";
import PropTypes from "prop-types";
import VideoItem from "./VideoItem";

class VideoFeed extends Component {
  render() {
    const { videos } = this.props;
    return videos.map(video => <VideoItem key={video._id} video={video} />);
  }
}

VideoFeed.propTypes = {
  videos: PropTypes.array.isRequired
};

export default VideoFeed;
