import React, { Component } from "react";
import PropTypes from "prop-types";
import CompItem from "./CompItem";

class CompFeed extends Component {
  render() {
    const { competitions } = this.props;
    return competitions.map(competition => (
      <CompItem key={competition._id} competition={competition} />
    ));
  }
}

CompFeed.propTypes = {
  competitions: PropTypes.array.isRequired
};

export default CompFeed;
