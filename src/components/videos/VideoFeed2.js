import React, { Component } from "react";
import PropTypes from "prop-types";
import VideoItem2 from "./VideoItem2";

class VideoFeed2 extends Component {
  render() {
    const { videos } = this.props;
    return videos.map(video => <VideoItem2 key={video._id} video={video} />);
  }
}

VideoFeed2.propTypes = {
  videos: PropTypes.array.isRequired
};

export default VideoFeed2;
