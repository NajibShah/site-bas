import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { deleteComment } from '../../actions/competitionActions';
import { Comment, Tooltip, Avatar } from 'antd';

class CommentItemComp extends Component {
  onDeleteClick(competitionId, commentId) {
    this.props.deleteComment(competitionId, commentId);
  }

  render() {
    const { comment, competitionId, auth } = this.props;

    return (
      // <div className="card card-body mb-3">
      //   <div className="row">
      //     <div className="col-md-2">
      //       <a href="profile.html">
      //         <img
      //           className="rounded-circle d-none d-md-block"
      //           src={comment.avatar}
      //           alt=""
      //         />
      //       </a>
      //       <br />
      //       <p className="text-center">{comment.name}</p>
      //     </div>
      //     <div className="col-md-10">
      //       <p className="lead">{comment.text}</p>
      //       {comment.user === auth.user.id ? (
      //         <button
      //           onClick={this.onDeleteClick.bind(
      //             this,
      //             competitionId,
      //             comment._id
      //           )}
      //           type="button"
      //           className="btn btn-danger mr-1"
      //         >
      //           <i className="fas fa-times" />
      //         </button>
      //       ) : null}
      //     </div>
      //   </div>
      // </div>

      <div>
        {/* if user's own post */}
        {comment.user === auth.user.id && auth.user.name !== 'Filmabad' ? (
          <Comment
            author={<a style={{ color: 'pink' }}>{comment.name}</a>}
            avatar={<Avatar src={comment.avatar} alt={comment.name} />}
            content={<p style={{ color: 'white' }}>{comment.text}</p>}
            datetime={
              <Tooltip
                onClick={this.onDeleteClick.bind(
                  this,
                  competitionId,
                  comment._id
                )}
              >
                <span>Delete Comment</span>
              </Tooltip>
            }
          />
        ) : null}
        {/* if someone else's post */}
        {comment.user !== auth.user.id && auth.user.name !== 'Filmabad' ? (
          <Comment
            author={<a style={{ color: 'white' }}>{comment.name}</a>}
            avatar={<Avatar src={comment.avatar} alt={comment.name} />}
            content={<p style={{ color: 'white' }}>{comment.text}</p>}
          />
        ) : null}
        {/* if user is admin */}
        {auth.user.name === 'Filmabad' ? (
          <Comment
            author={<a style={{ color: 'white' }}>{comment.name}</a>}
            avatar={<Avatar src={comment.avatar} alt={comment.name} />}
            content={<p style={{ color: 'white' }}>{comment.text}</p>}
            datetime={
              <Tooltip
                onClick={this.onDeleteClick.bind(
                  this,
                  competitionId,
                  comment._id
                )}
              >
                <span>Delete Comment</span>
              </Tooltip>
            }
          />
        ) : null}
      </div>
    );
  }
}

CommentItemComp.propTypes = {
  deleteComment: PropTypes.func.isRequired,
  comment: PropTypes.object.isRequired,
  competitionId: PropTypes.string.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { deleteComment }
)(CommentItemComp);
