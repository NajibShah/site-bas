import React, { Component } from "react";
import PropTypes from "prop-types";
import EntryItem from "./EntryItem";

export class EntryFeed extends Component {
  render() {
    const { entries, competitionId } = this.props;
    return entries.map(entry => (
      <EntryItem key={entry._id} entry={entry} competitionId={competitionId} />
    ));
  }
}

EntryFeed.propTypes = {
  entries: PropTypes.array.isRequired,
  competitionId: PropTypes.string.isRequired
};

export default EntryFeed;
