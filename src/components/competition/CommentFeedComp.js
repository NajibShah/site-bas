import React, { Component } from "react";
import PropTypes from "prop-types";
import CommentItemComp from "./CommentItemComp";

class CommentFeedComp extends Component {
  render() {
    const { comments, competitionId } = this.props;

    return comments.map(comment => (
      <CommentItemComp
        key={comment._id}
        comment={comment}
        competitionId={competitionId}
      />
    ));
  }
}

CommentFeedComp.propTypes = {
  comments: PropTypes.array.isRequired,
  competitionId: PropTypes.string.isRequired
};

export default CommentFeedComp;
