import React, { Component } from "react";
import PropTypes from "prop-types";
import CommentItem from "./CommentItem";

class CommentFeed extends Component {
  render() {
    const { comments, videoId } = this.props;

    return comments.map((comment) => (
      <CommentItem key={comment._id} comment={comment} videoId={videoId} />
    ));
  }
}

CommentFeed.propTypes = {
  comments: PropTypes.array.isRequired,
  videoId: PropTypes.string.isRequired,
};

export default CommentFeed;
