/* this prevents us from having to manually make sure that we have the token for each request that we need 
this function when called is just going to always attach the authorization header */
import axios from "axios";

const setAuthToken = token => {
  if (token) {
    //Apply to every request
    axios.defaults.headers.common["Authorization"] = token;
  } else {
    // if token isnt there Delete auth header
    delete axios.defaults.headers.common["Authorization"];
  }
};

export default setAuthToken;
